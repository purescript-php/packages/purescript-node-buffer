<?php

$exports['byteLengthImpl'] = function ($str) {
  return function ($enc) use (&$str) {
    return mb_strlen($str, $enc);
  };
};
